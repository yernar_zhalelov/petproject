import { NavigationContainer } from '@react-navigation/native';
import React, {useEffect} from 'react';
import NavigatorView from './src/navigation/NavigatorView';

import messaging from '@react-native-firebase/messaging'

import AsyncStorage from '@react-native-async-storage/async-storage';

import {url} from './src/AppConfig'

function App() {
  useEffect(() => {

    getPermission()

    messaging().onMessage(async remoteMessage => {
      console.log("Message inside app = ", remoteMessage.notification)
    })

    AsyncStorage.getItem('token').then((token) => {
      if(token == null) {
        getToken()
      } 
    })

    
  },[])

  const getPermission = async () => {
    const status = await messaging().requestPermission()

  }

  const getFirebaseToken = async (token) => {
    const fcmToken = await messaging().getToken()
    console.log('Firebase token = ', fcmToken)

    AsyncStorage.setItem('fbToken', fcmToken)

    storeToken(token, fcmToken)
    
  }

  const getToken = () => {

    fetch(url + '/api/get-token', {
      headers: {
        'Content-Type':'text/html'
      },
      method: 'GET', 
    }). 
    then((response) => response.text()).
    then(responseJson => {
      
      AsyncStorage.setItem('token', responseJson)
      getFirebaseToken(responseJson)

    }).
    catch((error) => {
      console.log("Error in getting token = ", error)
    })
  }

  const storeToken = (token, key) => {

    fetch(url + `/api/store-token?token=${token}&device_key=${key}`, {
      headers: {
        'Content-Type':'text/html'
      },
      method: 'GET', 
    }). 
    then((response) => response.text()).
    then(responseJson => {
      
      AsyncStorage.setItem('notifs', 'true')

    }).
    catch((error) => {
      console.log("Error storing = ", error)
    })
  }

  return (
    <NavigationContainer>
      <NavigatorView />
    </NavigationContainer>
  )
}

export default App;

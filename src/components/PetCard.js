import React from "react";
import {
    View, 
    Text, 
    Image, 
    StyleSheet, 
    TouchableOpacity
} from 'react-native'
import { ifIphoneX } from "react-native-iphone-x-helper";
import Swipeable from 'react-native-gesture-handler/Swipeable';
import AsyncStorage from "@react-native-async-storage/async-storage";

import {url} from '../AppConfig'


const PetCard = ({item, navigation, ...props}) => {
    console.log(item)

    function renderImage(image) {
        if(image == null) {
            return require('../../assets/images/inputPhoto.png')
        } else {
            return {uri: image.url}
        }
    }

    function renderType(type) { 
        if(type == 1) {
            return 'Cat'
        } else {
            return 'Dog'
        }
    }

    function renderRightActions (item) {

        return (
          <TouchableOpacity
          style={{
            backgroundColor: '#FAAC75',
            justifyContent: 'center',
            alignItems: 'flex-end',
            alignSelf: 'flex-end',
            height: 75,
            borderTopRightRadius: 10, 
            borderBottomRightRadius: 10
          }}
          onPress={() => deletePet(item)}
        >
          <Text
            style={{
              color: '#FFFFFF',
              paddingHorizontal: 10,
              fontWeight: '600',
              paddingHorizontal: 30,
              paddingVertical: 20,
            }}
          >
            Delete
          </Text>
        </TouchableOpacity>
        );
    };

    function deletePet (item) {

        AsyncStorage.getItem('token').then((token) => {
          
          fetch(url + '/api/pets/' + item.id + '?token=' + token, {
            method: 'DELETE'
          }).
          then((response) => response.json()).
          then((responseJson) => {
            console.log(responseJson)
      
            let index = this.state.pets.indexOf(item)
            this.state.pets.splice(index, 1)
            
            this.setState({pets: this.state.pets})
      
          }).
          catch((error) => {
            console.error("Error in deleting = ", error)
          })
    
        })
    
      }

    return(
        <TouchableOpacity onPress={() => navigation.navigate('PetProfile', {pet: item})}>
            <Swipeable 
                renderRightActions={() => renderRightActions(item)}
                rightThreshold={10}
                overshootRight={false}
            >
                <View style={styles.petContainer}>

                    <Image 
                    source={renderImage(item.image)}
                    style={{
                        width: 60, 
                        height: 60,
                        marginHorizontal: 15,
                    }}
                    />

                    <View>

                        <Text style={{
                            color: '#333333', 
                            fontSize: 14,
                            fontFamily: 'Roboto-Medium', 
                        }}>
                            {item.name}
                        </Text>

                        <Text style={{
                            color: '#333333', 
                            fontFamily: 'Roboto-Regular', 
                            fontSize: 12
                        }}>
                            {renderType(item.type)}
                        </Text>

                    </View>

                </View>
            </Swipeable>
        </TouchableOpacity>
    )

}

const styles = StyleSheet.create({
    petContainer: {
        borderRadius: 10,
        backgroundColor: 'white', 
        height: 75, 
        width: '100%', 
        flexDirection: 'row', 
        alignItems: 'center', 
        marginTop: 15,
        ...ifIphoneX({
    
        }, {
          shadowOffset: {
            width: 0,
            height: 1,
          }, 
          shadowColor: '#000000', 
          shadowOpacity: 0.1,
          shadowRadius: 15,
          elevation: 0.5,
        }),
    },
})

export default PetCard;
import React from "react";
import {
    View, 
    Text,
    Image, 
    StyleSheet
} from 'react-native'

const EventCard = ({item, ...props}) => {


    return (
        <View style={styles.eventsContainer}>

            <View style={styles.greenCircleContainer}>
                <Image 
                    source={require('../../assets/images/greenCircle.png')}
                    style={{
                        margin: 10,
                    }}
                />
            </View>
            

            <Text style={styles.eventsText}>
                {item.name}
            </Text>

        </View>
    )

}

const styles = StyleSheet.create({
  eventsContainer: {
    backgroundColor: 'white', 
    borderRadius: 15,
    height: 75,
    flexDirection: 'row',
    justifyContent: 'center', 
    marginTop: 20,
  }, 
  greenCircleContainer: {
    flex: 1, 
  },
  eventsText: {
    fontSize: 16, 
    fontFamily: 'Roboto-Regular', 
    color: '#333333',  
    flex: 7,
    marginTop: 10,
  },
})

export default EventCard; 
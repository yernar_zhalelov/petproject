import React, {useState} from 'react'
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  TouchableOpacity, 
  TextInput, 
  Alert, 
  Platform, 
  ToastAndroid,
  Keyboard,
  Vibration
} from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper';
import {Picker} from '@react-native-picker/picker';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePickerModal from "react-native-modal-datetime-picker";


const width = Dimensions.get('screen').width
const height = Dimensions.get('screen').height

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];


const Procedure = ({modalOpen, procedure, name, changeName, closeProcedure, setLastProcedureDate, setLastProcedureTime, setFreqNumber, setFreqPeriod, setDose, ...props}) => {
    const [number, setNumber] = useState(1)
    const [period, setPeriod] = useState('1')
    const [day, setDay] = useState(null)
    const [month, setMonth]= useState(null)
    const [year, setYear] = useState(null)
    const [hour, setHour]= useState(null)
    const [minute, setMinute] = useState(null)
    const [dateVisible, setDateVisible] = useState(false)
    const [timeVisible, setTimeVisible] = useState(false)

    const pickerAndroid = () => {
        var result = []

        for(var i = 1; i < 100; i++) {
            result.push(
                <Picker.Item label={i.toString()} value={i}/>
            )
        }

        return result
    }

    const pickerIos = () => {
        var result = []

        for(var i = 1; i < 100; i++) {
            result.push({label: i.toString(), value: i})
        }

        return result
    }

    const renderPickerNumber = () => {
        if(Platform.OS == 'android') {
            return (
                <Picker
                selectedValue={number}
                onValueChange={(itemValue, itemIndex) =>
                    {
                        setNumber(itemValue)
                        setFreqNumber(itemValue)
                    }
                }>
                    {pickerAndroid()}
                </Picker>
            )
        } else {
            return (
                <RNPickerSelect
                    value={number}
                    onValueChange={(value) => {
                        setNumber(value)
                        setFreqNumber(value)
                    }}
                    items={pickerIos()}
                    style={{
                        inputIOS: {
                            alignSelf: 'center'
                        }
                    }}
                />
            )
        }
    }

    const renderPickerPeriod = () => {
        if(Platform.OS == 'android') {
            return (
                <Picker
                selectedValue={period}
                onValueChange={(itemValue, itemIndex) =>
                    {
                        setPeriod(itemValue)
                        setFreqPeriod(itemValue)
                    }
                }>
                    <Picker.Item label={'a day'} value={'1'}/>
                    <Picker.Item label={'a week'} value={'2'}/>
                    <Picker.Item label={'a month'} value={'3'}/>
                    <Picker.Item label={'a quarter'} value={'4'}/>
                    <Picker.Item label={'a year'} value={'5'}/>
                </Picker>
            )
        } else {
            return (
                <RNPickerSelect
                    value={period}
                    onValueChange={(value) => {
                        setFreqPeriod(value)
                        setPeriod(value)
                    }}
                    items={[
                        {label: 'a day', value: '1'},
                        {label: 'a week', value: '2'},
                        {label: 'a month', value: '3'},
                        {label: 'a quarter', value: '4'},
                        {label: 'a year', value: '5'},
                    ]}
                    style={{
                        inputIOS: {
                            alignSelf: 'center'
                        }
                    }}
                    label={period}
                />
            )
        }
    }

    const handleDatePicker = (date) => {
        let chosendDate = new Date(date)

        setDay(chosendDate.getDate())
        setMonth(monthNames[chosendDate.getMonth()])

        setYear(chosendDate.getFullYear())
        setLastProcedureDate(`${chosendDate.getFullYear()}-${chosendDate.getMonth() + 1}-${chosendDate.getDate()}`)
        
        setDateVisible(false)

    }

    const handleTimePicker = (date) => {
        let chosendDate = new Date(date)

        setHour(String(chosendDate.getHours()).padStart(2, "0"))
        setMinute(String(chosendDate.getMinutes()).padStart(2, "0"))

        setLastProcedureTime(`${String(chosendDate.getHours()).padStart(2, "0")}:${String(chosendDate.getMinutes()).padStart(2, "0")}`)

        setTimeVisible(false)
    }

    return(
        <View style={styles.container}>

            <View style={styles.body}>

                <View style={[styles.categoryContainer, {flexDirection: 'row', justifyContent: 'space-between', height: 20}]}>

                    <Text style={styles.title}>
                        Procedure
                    </Text>

                    <TouchableOpacity style={styles.delete} onPress={closeProcedure}>
                        <Image 
                            source={require('../../assets/images/delete.png')}
                        />
                    </TouchableOpacity>

                </View>

                <View style={styles.categoryContainer}>

                    <Text style={styles.category}>
                        Procedure
                    </Text>

                    <TouchableOpacity style={styles.chooseProcedure} onPress={modalOpen}>
                        <Text >
                            {procedure.name}
                        </Text>

                        <Image 
                            source={require('../../assets/images/modalButton.png')}
                        />

                    </TouchableOpacity>

                </View>

                <View style={styles.categoryContainer}>

                    <Text style={styles.category}>
                        Name
                    </Text>

                    <TextInput 
                        style={styles.nameInput}
                        onChangeText={(text) => changeName(text)}
                    />

                </View>

                
                <View style={styles.categoryContainer}>

                    <Text style={styles.category}>
                        Frequency
                    </Text>

                    <View style={styles.frequency}>
                        <View style={styles.frequencyNumber}>
                            
                            {renderPickerNumber()}

                        </View>
                        

                        <Text style={styles.timeText}>
                            time
                        </Text>

                        <View style={styles.frequencyPeriod}>
                            {renderPickerPeriod()}
                        </View>
                    </View>

                </View>

                <View style={styles.categoryContainer}>

                    <Text style={[styles.category]}>
                        Dose (not mandatory)
                    </Text>

                    <TextInput 
                        style={styles.nameInput}
                        onChangeText={(text) => setDose(text)}
                    />

                </View>

                <View style={styles.categoryContainer}> 

                    <Text style={[styles.category]}>
                        Date of the last procedure
                    </Text>

                    <View style={styles.lastProcedureDate}>

                        <TouchableOpacity style={styles.dateContainer} onPress={() => setDateVisible(true)}>

                            <Text style={styles.dateText}>
                                {day}
                            </Text>

                        </TouchableOpacity>

                        <TouchableOpacity style={styles.dateContainer} onPress={() => setDateVisible(true)}>

                            <Text style={styles.dateText}>
                                {month}
                            </Text>

                        </TouchableOpacity>

                        <TouchableOpacity style={styles.dateContainer} onPress={() => setDateVisible(true)}>

                            <Text style={styles.dateText}>
                                {year}
                            </Text>

                        </TouchableOpacity>
                    
                    </View>

                </View>

                <View style={styles.categoryContainer}>

                    <Text style={[styles.category]}>
                        Time of the last procedure
                    </Text>

                    <View style={styles.lastProcedureTime}>

                        <TouchableOpacity style={styles.timeContainer} onPress={() => setTimeVisible(true)}>

                            <Text style={styles.timeText}>
                                {hour}
                            </Text>

                        </TouchableOpacity>

                        <TouchableOpacity style={styles.timeContainer} onPress={() => setTimeVisible(true)}>

                            <Text style={styles.timeText}>
                                {minute}
                            </Text>

                        </TouchableOpacity>

                    </View>

                </View>

                

            </View>

            <DateTimePickerModal
                isVisible={dateVisible}
                mode="date"
                onConfirm={(date) => handleDatePicker(date)}
                onCancel={() => setDateVisible(false)}
                date={new Date()}
            />

            <DateTimePickerModal
                isVisible={timeVisible}
                mode="time"
                onConfirm={(date) => handleTimePicker(date)}
                onCancel={() => setTimeVisible(false)}
            />

        </View>
    )
}



const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white', 
        borderRadius: 15, 
        width: width * 0.9, 
        marginTop: 10, 
        alignSelf: 'center'
    }, 
    body: {
        width: '90%', 
        alignSelf: 'center',
        flex: 1
    },
    categoryContainer: {
        width: '100%',
        alignSelf: 'center', 
        marginTop: 10,
        height: 80, 
    }, 
    title: {
        color: '#333333', 
        fontSize: 18, 
        fontFamily: 'Roboto-Medium', 
        alignSelf: 'center'
    }, 
    delete: {
        alignSelf: 'center'
    }, 
    category: {
        marginTop: 10,
        fontSize: 16,
        fontFamily: 'Roboto-Regular',
        color: '#333333'
    }, 
    chooseProcedure: {
        borderBottomColor: '#DBD5D5', 
        borderBottomWidth: 1,
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        marginTop: 10,
    }, 
    nameInput: {
        borderBottomColor: '#DBD5D5', 
        borderBottomWidth: 1,
        color: 'black'
    },
    frequency: {
        flexDirection: 'row',
        justifyContent: 'space-between', 
        marginTop: 10,
    },
    frequencyNumber: {
        width: 100,
        height: 35,
        borderRadius: 10,
        borderColor: '#C4C4C4', 
        borderWidth: 1,
        justifyContent: 'center'
    }, 
    frequencyPeriod: {
        width: 140, 
        height: 35,
        borderRadius: 10,
        borderColor: '#C4C4C4', 
        borderWidth: 1,
        justifyContent: 'center'
    }, 
    timeText: {
        color: '#333333', 
        fontSize: 16,
        fontFamily: 'Roboto-Regular', 
        alignSelf: 'center'
    }, 
    lastProcedureDate: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        marginTop: 10
    },
    lastProcedureTime: {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        width: '65%',
        marginTop: 10
    }, 
    dateContainer: {
        width: '30%', 
        height: 35, 
        borderColor: '#C4C4C4', 
        borderWidth: 1,
        borderRadius: 10, 
        justifyContent: 'center'
    }, 
    dateText: {
        fontFamily: 'Roboto-Regular', 
        fontSize: 16, 
        alignSelf: 'center', 
        color: '#000000'
    },
    timeContainer: {
        width: '45%', 
        height: 35, 
        borderColor: '#C4C4C4', 
        borderWidth: 1,
        borderRadius: 10, 
        justifyContent: 'center'
    }, 
    timeText: {
        fontFamily: 'Roboto-Regular', 
        fontSize: 16, 
        alignSelf: 'center', 
        color: '#000000'
    }
})

export default Procedure
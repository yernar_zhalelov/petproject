import React, {useState, useRef, useEffect} from 'react';
import { Text, View, Image, StyleSheet, Dimensions } from 'react-native';
import { createBottomTabNavigator, BottomTabBar } from '@react-navigation/bottom-tabs';
import { SvgUri } from 'react-native-svg';

import MainScreen from '../screens/main/Main';
import SettingsScreen from '../screens/settings/Settings';
import PetPage from './TabIcon/petPage';
import Settings from './TabIcon/settings';

const petPage = require('../../assets/images/petPage.png')
const settings = require('../../assets/images/settings.png')

const Tab = createBottomTabNavigator()

const Tabs = () => {
    return (
        <Tab.Navigator
        >

            <Tab.Screen 
                name='Main'
                component={MainScreen}

                options={{
                    header: () => {return null},
                    tabBarIcon: ({focused}) => (
                        <View>
                            <PetPage 
                                color={focused ? "#EFAF7E" : '#A4A0A0'}
                            />
                        </View>
                    ), 
                    tabBarLabel: () =>{return null}
                }}
            />

            <Tab.Screen 
                name='Settings'
                component={SettingsScreen}

                options={{
                    header: () => {return null},
                    tabBarIcon: ({focused}) => (
                        <View>
                            <Settings 
                                color={focused ? "#EFAF7E" : '#A4A0A0'}
                            />
                        </View>
                    ), 
                    tabBarLabel: () =>{return null}
                }}
            />
        </Tab.Navigator>
    )
}

const styles = StyleSheet.create({
    tabBarIconFocused: {
        tintColor: "#EFAF7E"
    },
    tabBarIcon: {
        tintColor: '#A4A0A0',
      },
})

export default Tabs
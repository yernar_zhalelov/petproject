import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { TabBarIOS } from "react-native";
import AddPetScreen from "../addPet/addPet";
import AddProcedureScreen from "../screens/addProcedure/addProcedure";
import AllPetsScreen from "../screens/allPets/allPets";
import FinalPageScreen from "../screens/finalPage/FinalPage";
import LoaderScreen from "../screens/loader/Loader";
import PetProfileScreen from "../screens/petProfile/PetProfile";
import Tabs from "./TabNavigator";
import UpdatePetScreen from "../screens/updatePet/updatePet";

const stackNavigationData = [
    {
        name: 'Loader', 
        component: LoaderScreen
    }, 
    {
        name: 'Tab', 
        component: Tabs
    }, 
    {
        name: 'AddPet', 
        component: AddPetScreen
    },
    {
        name: 'AddProcedure', 
        component: AddProcedureScreen
    }, 
    {
        name: 'AllPets', 
        component: AllPetsScreen
    }, 
    {
        name: 'FinalPage', 
        component: FinalPageScreen
    }, 
    {
        name: 'PetProfile', 
        component: PetProfileScreen
    },
    {
        name: 'UpdatePet', 
        component: UpdatePetScreen
    },
]

const Stack = createStackNavigator()

export default function NavigatorView(props) {
    return (
        <Stack.Navigator>
            {stackNavigationData.map((item, index) => (
                <Stack.Screen 
                    name={item.name}
                    component={item.component}

                    options={({ route }) => ({
                        headerTransparent: true,
                        headerTitle: ' ',
                        headerLeft: null,
                    })}
                    key={index}
                />
            ))}
        </Stack.Navigator>
    )
}
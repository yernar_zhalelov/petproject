import React from 'react'
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Image
} from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class LoaderScreen extends React.Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.reset({
        index: 0,
        routes: [{name: 'Tab'}],
      })
    }, 500)
  }

  render() {
    return (
      <View style={styles.container}>
          <View style={styles.body}>

              <Text style={styles.titleText}>Fluffy Friend</Text>

              <View style={styles.imageContainer}>

                  <Image 
                    source={require('../../../assets/images/cat.png')}
                    style={{
                        position: 'absolute', 
                        bottom: 0,
                        marginLeft: -10
                    }}
                  />

                  <Image 
                    source={require('../../../assets/images/dog.png')}
                    style={{
                        
                    }}
                  />

              </View>

              <Text style={styles.text}>
                  YOUR PET'S HEALTH IS UNDER CONTROL
              </Text>

              <Image 
                source={require('../../../assets/images/petPrint.png')}
                style={{
                    position: 'absolute', 
                    bottom: height * 0.11,
                    left: width * 0.2,
                }}
              />

              <Image 
                source={require('../../../assets/images/petPrint.png')}
                style={{
                    position: 'absolute', 
                    top: 10,
                    left: width * 0.3,
                }}
              />

              <Image 
                source={require('../../../assets/images/petPrint.png')}
                style={{
                    position: 'absolute', 
                    top: height * 0.15,
                    left: width * 0.15,
                }}
              />

          </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#F5F5F9', 
    justifyContent: 'flex-start'
  }, 
  body: {
      ...ifIphoneX({
          marginTop: 50,
      }),
      height: height * 0.8,
      justifyContent: 'space-around'
  }, 
  titleText: {
    color: '#FAAC75', 
    fontFamily: 'Roboto-Bold',
    fontSize: 48, 
    alignSelf: 'center'
  }, 
  imageContainer: {
    width: width * 0.85, 
    alignSelf: 'center'
  }, 
  text: {
      fontFamily: 'Roboto-Black', 
      color: '#FAAC75',
      fontSize: 18, 
      width: width * 0.7, 
      alignSelf: 'center', 
      textAlign: 'center', 
  }
})

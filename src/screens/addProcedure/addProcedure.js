import React, {useRef} from 'react'
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  TouchableOpacity, 
  TextInput, 
  Alert, 
  Platform, 
  ToastAndroid,
  Keyboard, 
  SafeAreaView,
  ScrollView
} from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { Modalize } from 'react-native-modalize';
import RNPickerSelect from 'react-native-picker-select';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {url} from '../../AppConfig'

import {
  InterstitialAd,
  AdEventType,
  TestIds
} from '@react-native-firebase/admob';

import DateTimePickerModal from "react-native-modal-datetime-picker";

import Procedure from '../../components/Procedure' 

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

const interstitialAd = InterstitialAd.createForAdRequest(TestIds.INTERSTITIAL);

export default class AddProcedureScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state={
      name: '',
      cat: false,
      dog: false, 
      image: '',
      keyboard: false,
      procedure: false,
      choosenProcedure: {name: '', id: 0},
      procedureName: null, 
      dose: 0, 
      freqNumber: null, 
      freqPeriod: null, 
      lastProcedureDate: null, 
      lastProcedureTime: null,
      petName: props.route.params.petName, 
      petType: props.route.params.petType, 
      petImage: props.route.params.petImage,
      petId: 0, 
    }

    this.modalizeRef = React.createRef(<Modalize></Modalize>)
  }

  componentDidMount() {

    interstitialAd.onAdEvent(type => {
      if (type === AdEventType.LOADED) {
        console.log('InterstitialAd adLoaded');
      } else if (type === AdEventType.ERROR) {
        console.warn('InterstitialAd => Error');
      } else if (type === AdEventType.OPENED) {
        console.log('InterstitialAd => adOpened');
      } else if (type === AdEventType.CLICKED) {
        console.log('InterstitialAd => adClicked');
      } else if (type === AdEventType.LEFT_APPLICATION) {
        console.log('InterstitialAd => adLeft_App');
      } else if (type === AdEventType.CLOSED) {
        console.log('InterstitialAd => adClosed');
        interstitialAd.load();
      }
    });

    interstitialAd.load();

  }

  showAd() {
    if (interstitialAd.loaded) {
      interstitialAd.show().catch(error => console.warn(error));
    }
  }

  createProcedure = (id) => {
    console.log(id)
    
    AsyncStorage.getItem('token').then((token) => {
      
      var dataform = new FormData();
      dataform.append('pet_id', id)
      dataform.append('procedure_type', this.state.choosenProcedure.id)
      dataform.append('name', this.state.procedureName)
      dataform.append('frequency_number', this.state.freqNumber)
      dataform.append('frequency_type', this.state.freqPeriod)
      dataform.append('last_procedure', this.state.lastProcedureDate + ' ' + this.state.lastProcedureTime)
      dataform.append('dose', this.state.dose)

      console.log('iD == ', id)

      fetch(url + '/api/procedures?token=' + token, {
        method: 'POST', 
        headers: {
          'Content-Type' : 'multipart/form-data'
        }, 
        body: dataform
      }). 
      then((response) => response.json()).
      then((responseJson) => {
        
        console.log(responseJson)


        this.props.navigation.navigate('FinalPage')
      }).
      catch((error) => {
        console.log("Error = ", error)
      })

    })

  }

  createPet = () => {
    AsyncStorage.getItem('token').then((token) => {
      console.log(token)
      
      var dataform = new FormData();
      dataform.append('name', this.state.petName)
      dataform.append('type', 1)
      dataform.append('token', token)

      fetch(url + '/api/pets', {
        method: 'POST', 
        headers: {
          'Content-Type' : 'multipart/form-data'
        }, 
        body: dataform
      }). 
      then((response) => response.json()).
      then((responseJson) => {
        console.log(responseJson)
        
        if(responseJson.message == 'Pet saved') {

          if(this.state.petImage != '') {

            this.sendImage(responseJson.id)
            this.setState({petId: responseJson.id})

          }

          if(this.state.procedure == true) {
            this.createProcedure(responseJson.id)
          } else {
            this.props.navigation.navigate('FinalPage')
          }
        } else {
          ToastAndroid.show(responseJson, ToastAndroid.SHORT)
        }
      }).
      catch((error) => {
        console.log("Error = ", error)
      })

    })
    
  }

  sendImage = (id) => {
    var imageForm = new FormData()

    var sendImage = {
      uri: this.state.petImage.uri,
      type: this.state.petImage.type, 
      name: this.state.petImage.fileName
    }

    imageForm.append('file', sendImage)

    fetch(url + '/api/pets/image/' + id, {
      method: 'POST', 
      headers: {
        'Content-Type' : 'multipart/form-data'
      }, 
      body: imageForm
    }). 
    then((response) => response.json()).
    then((responseJson) => {
      console.log("image response = ", responseJson)
    }).
    catch((error) => {
      console.log("Error = ", error)
    })
  }

  onOpen = () => {
    this.modalizeRef.current?.open();
  };

  renderProcedure = () => {
    if(this.state.procedure == true) {
      return (
        <Procedure 
          key={1}
          modalOpen={() => this.onOpen()}
          procedure={this.state.choosenProcedure}
          closeProcedure={() => this.setState({procedure: false})}
          setLastProcedureDate={(item) => this.setState({lastProcedureDate: item})}
          setLastProcedureTime={(item) => this.setState({lastProcedureTime: item})}
          changeName={(item) => this.setState({procedureName: item})}
          setFreqNumber={(item) => this.setState({freqNumber: item})}
          setFreqPeriod={(item) => this.setState({freqPeriod: item})}
          setDose={(item) => this.setState({dose: item})}
        />
      )
    } else {
      return null
    }
  }

  renderTick = (item) => {
    if(this.state.choosenProcedure == item) {
      return (
        <Image 
          source={require('../../../assets/images/tick.png')}
          style={{
            alignSelf: 'center', 
          }}
        />
      )
    } else {
      return null
    }
  }

  setLastPDate = (date) => {
    console.log(date)
  }

  setLastPTime = (time) => {
    console.log(time)
  }

  initializingPet = () => {
    this.showAd()

    if(this.state.procedure == false) {

      this.createPet()

    } else {

      if(
        this.state.choosenProcedure == null || this.state.choosenProcedure.name.trim() == '' ||
        this.state.procedureName == null || this.state.procedureName.trim() == '' ||
        this.state.freqNumber == null ||
        this.state.freqPeriod == null || this.state.freqPeriod.trim() == '' ||
        this.state.lastProcedureDate == null || this.state.lastProcedureDate.trim() == '' ||
        this.state.lastProcedureTime == null || this.state.lastProcedureTime.trim() == '' 
      ) {
        console.log(this.state.choosenProcedure)
        console.log(this.state.procedureName)
        console.log(this.state.freqNumber)
        console.log(this.state.freqPeriod)
        console.log(this.state.lastProcedureDate)
        console.log(this.state.lastProcedureTime)
        
        if(Platform.OS == 'android') {
          ToastAndroid.show('Fill in required fields', ToastAndroid.SHORT)
        } else {
          Alert.alert('Fill in required fields')
        }
      } else {
        this.createPet()
      }

    }

    
      
    
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{flex: 1}}>
        <View style={styles.body}>

          <View style={styles.header}>

          <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{alignSelf: 'center', height: 40, width: 40, justifyContent: 'center'}}>
                <Image 
                    source={require('../../../assets/images/backButton.png')}
                    style={{
                        height: 20, 
                        width: 10,
                        alignSelf: 'center',
                    }}
                />
            </TouchableOpacity>
              

            <Text style={styles.addPetText}>
                Add a pet
            </Text>

          </View>

          <Text style={styles.informationText}>
            Step 2/2: procedures
          </Text>

          <Text style={styles.subtitleText}>
              You can add procedures now or later
          </Text>


          <TouchableOpacity style={styles.addButton} onPress={() => this.setState({procedure: true})}>
            <Text style={styles.addButtonText}>
              Add
            </Text>
          </TouchableOpacity>

          {this.renderProcedure()}

          <TouchableOpacity style={styles.saveButton} onPress={() => this.initializingPet()}>
            <Text style={styles.saveText}>
              Save
            </Text>
          </TouchableOpacity>

          
          
        </View>
        </ScrollView>

        <Modalize ref={this.modalizeRef} modalHeight={height * 0.35}>

          <View style={{
            width: '90%', 
            marginTop: 20, 
            alignSelf: 'center', 
          }}>

            <Text style={{
              fontSize: 18, 
              color: '#333333', 
              fontFamily: 'Roboto-Medium'
            }}>
              Procedure type
            </Text>

            <TouchableOpacity style={styles.procedureType} onPress={() => this.setState({choosenProcedure: {name: 'Vaccination', id: 1}})}>
              <View style={{flexDirection: 'row'}}>
                <Image 
                  source={require('../../../assets/images/vaccination.png')}
                  style={{

                  }}
                />

                <Text style={styles.procedureText}>
                  Vaccination
                </Text>
              </View>
              

              {this.renderTick('Vaccination')}
            </TouchableOpacity>

            <TouchableOpacity style={styles.procedureType} onPress={() => this.setState({choosenProcedure: {name: 'Deworming', id: 2}})}>
              <View style={{flexDirection: 'row'}}> 
                <Image 
                  source={require('../../../assets/images/deworming.png')}
                  style={{

                  }}
                />

                <Text style={styles.procedureText}>
                  Deworming
                </Text>
              </View>
              

              {this.renderTick('Deworming')}
            </TouchableOpacity>

            <TouchableOpacity style={styles.procedureType} onPress={() => this.setState({choosenProcedure: {name: 'Medication', id: 3}})}>
              <View style={{flexDirection: 'row'}}>
                <Image 
                  source={require('../../../assets/images/medication.png')}
                  style={{

                  }}
                />

                <Text style={styles.procedureText}>
                  Medication
                </Text>
              </View>
              

              {this.renderTick('Medication')}
            </TouchableOpacity>

            <TouchableOpacity style={styles.procedureType} onPress={() => this.setState({choosenProcedure: {name: 'Grooming', id: 4}})}>
              <View style={{flexDirection: 'row'}}>
                <Image 
                  source={require('../../../assets/images/grooming.png')}
                  style={{

                  }}
                />

                <Text style={styles.procedureText}>
                  Grooming
                </Text>
              </View>
              

              {this.renderTick('Grooming')}
            </TouchableOpacity>

          </View>
            

        </Modalize>

      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#F5F5F9', 
    justifyContent: 'flex-start'
  }, 
  body: {
    ...ifIphoneX({
        marginTop: 50,
    }),
    width: width * 0.9, 
    alignSelf: 'center', 
    flex: 1,
  }, 
  header: {
    flexDirection: 'row',
    textAlign: 'center',
    marginTop: 30,
    
  }, 
  addPetText: {
    color: '#333333', 
    fontSize: 24,
    fontFamily: 'Roboto-Medium', 
    alignSelf: 'center', 
    marginLeft: 50,
  }, 
  informationText: {
    fontSize: 18,
    color: '#333333', 
    fontFamily: 'Roboto-Medium',
    marginTop: 20,
  }, 
  subtitleText: {
    color: '#727070',
    marginTop: 5,
  },
  saveButton: {
    borderRadius: 10,
    height: 54, 
    backgroundColor: '#FAAC75', 
    shadowOffset: {
      width: 0,
      height: 4,
    }, 
    shadowColor: '#000000', 
    shadowOpacity: 0.5,
    shadowRadius: 5,
    justifyContent: 'center', 
    // elevation: 10,
    // position: 'absolute', 
    // bottom: 35,
    width: '90%', 
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 30
  }, 
  saveText: {
    fontFamily: 'Roboto-Medium', 
    color: '#333333', 
    fontSize: 18,
    alignSelf: 'center'
  },
  addButton: {
    width: width * 0.8, 
    height: 40, 
    backgroundColor: '#FFDCC3', 
    borderRadius: 10,
    alignSelf: 'center', 
    marginTop: 10,
    justifyContent: 'center'
  }, 
  addButtonText: {
    color: '#333333', 
    fontSize: 14,
    fontFamily: 'Roboto-Bold',
    alignSelf: 'center'
  }, 
  procedureType: {
    flexDirection: 'row', 
    borderBottomColor: '#DBD5D5', 
    borderBottomWidth: 1,
    marginTop: 20,
    paddingBottom: 10,
    justifyContent: 'space-between'
  }, 
  procedureText: {
    fontSize: 16, 
    fontFamily: 'Roboto-Regular', 
    color: '#333333', 
    marginLeft: 15
  }
})

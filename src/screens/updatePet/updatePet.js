
import React from 'react'
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  TouchableOpacity, 
  TextInput, 
  Alert, 
  Platform, 
  ToastAndroid,
  Keyboard
} from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

import { url } from '../../AppConfig';
import AsyncStorage from '@react-native-async-storage/async-storage';


const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

let options = {
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

let options2 = {
  title: 'Select Image',
  customButtons: [
    { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
  ],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export default class UpdatePetScreen extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    this.state={
      name: props.route.params.pet.name,
      cat: false,
      dog: false, 
      image: props.route.params.pet.image,
      keyboard: false,
      type: 0,
    }
  }

  componentDidMount() {
    Keyboard.addListener('keyboardDidShow', () => {
      this.setState({keyboard: true})
    })

    Keyboard.addListener('keyboardDidHide', () => {
      this.setState({keyboard: false})
    })

    if(this.props.route.params.pet.type == 1) {
        this.setState({cat: true})
    } else {
        this.setState({dog: true})
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  createPet = () => {
    let type;

    if(this.state.cat == true) {
      type = 1
    } else if(this.state.dog == true) {
      type = 2
    }

    this.props.navigation.navigate('AddProcedure', {
      petName: this.state.name, 
      petType: type, 
      petImage: this.state.image
    })
  }

  renderSourceImage = () => {
    if (this.state.image == '') {
      return require('../../../assets/images/inputPhoto.png')
    } else {

        let temp = JSON.stringify(this.state.image)
    
        if(temp.includes('uri')) {
            return {uri: this.state.image.uri}
        } else {
            return {uri: this.state.image.url}
        }

      
    }
  }

  camera = () => {
    launchCamera(options, (response) => {

      if(response.errorCode) {

        if(Platform.OS == 'android') {
          ToastAndroid.show('Error', ToastAndroid.SHORT)
        } else {
          Alert.alert('Error')
        }

      } else {
        console.log("image = ", response.assets[0])
        this.setState({image: response.assets[0]})
      }

    })
  }

  library = () => {
    launchImageLibrary(options2, (response) => {
      if(response.errorCode) {

        if(Platform.OS == 'android') {
          ToastAndroid.show('Error', ToastAndroid.SHORT)
        } else {
          Alert.alert('Error')
        }

      } else if(response.didCancel) {
        console.log("User cancelled")
      } else {
        this.setState({image: response.assets[0]})
      }

    })
  }

  showAlert = () => {
    Alert.alert(
      'Choose one of options',
      'hello, my friend',
      [
        {
          text: "Камера",
          onPress: () => this.camera()
        }, 
        {
          text: "Библиотека",
          onPress: () => this.library()
        }, 
        {
          text: "Отмена",
          onDismiss: () => console.log("Отмена")
        }
      ], 
      {
        cancelable: true, 
        onDismiss: () => console.log('On dismiss')
      }
    )
  }

  getBackgroundColor = (item) => {
    if(item === 'cat') {
        if(this.state.cat == true) {
            return '#FFDCC3'
        } else {
            return '#D7D5D5'
        }
    } else {
        if(this.state.dog == true) {
            return '#FFDCC3'
        } else {
            return '#D7D5D5'
        }
    }
  }

  getNextBackground = () => {
    if((this.state.dog == false && this.state.cat == false) || this.state.name == '') {
      return '#D7D5D5'
    } else {
      return '#FAAC75'
    }
  }

  getTintColor = (item) => {
    if(item === 'cat') {
        if(this.state.cat == true) {
            return '#FAAC75'
        } else {
            return '#A5A0A0'
        }
    } else {
        if(this.state.dog == true) {
            return '#FAAC75'
        } else {
            return '#A5A0A0'
        }
    }
  }

  chooseType = (item) => {
    if(item == 'cat') {
        if(this.state.cat == false) {
          this.setState({cat: true})
          this.setState({dog: false})
        } else {
          this.setState({cat: false})
          this.setState({dog: false})
        }
    } else {
        if(this.state.dog == false) {
          this.setState({dog: true})
          this.setState({cat: false})
        } else {
          this.setState({cat: false})
          this.setState({dog: false})
        }
    }
  }

  renderKeyboardButton = () => {
    if(this.state.keyboard == false) {
      return <View>

      </View>
    } else {
      return (
        <TouchableOpacity onPress={() => Keyboard.dismiss()}>
          <Image 
            source={require('../../../assets/images/keyboardHide.png')}
            style={{
              ...ifIphoneX({
                marginLeft: 40,
              }, {
                marginLeft: 25
              })
            }}
          />
        </TouchableOpacity>
      )
    }
  }

  handleNext = () => {
    if((this.state.dog == false && this.state.cat == false) || this.state.name == '' || this.state.name.trim() == '') {
      console.log("Not all")
    } else {

        AsyncStorage.getItem('token').then((token) => {
            var dataform = new FormData();

            dataform.append('name', this.state.name)

            if(this.state.cat == true) {
                dataform.append('type', 1)
            } else {
                dataform.append('type', 2)
            }

        
            dataform.append('token', token)

            this.sendImage(this.props.route.params.pet.id)
            

            fetch(url + '/api/pets/' + this.props.route.params.pet.id, {
                headers: {
                'Content-Type':'multipart/form-data'
                },
                method: 'POST',
                body: dataform 
            }). 
            then((response) => response.text()).
            then(responseJson => {

                this.sendImage(this.props.route.params.pet.id)
                
                this.props.navigation.reset({
                    index: 0,
                    routes: [{name: 'Tab'}],
                })
        
            }).
            catch((error) => {
                console.log("Error in getting token = ", error)
            })
        })
        
    }
  }

  sendImage = (id) => {

    if(this.state.image == this.props.route.params.pet.image) {
        console.log("NO new image")
    } else {
        var imageForm = new FormData()

        var sendImage = {
          uri: this.state.image.uri,
          type: this.state.image.type, 
          name: this.state.image.fileName
        }

        console.log(sendImage)

        imageForm.append('file', sendImage)

        fetch(url + '/api/pets/image/' + id, {
          method: 'POST', 
          headers: {
            'Content-Type' : 'multipart/form-data'
          }, 
          body: imageForm
        }). 
        then((response) => response.json()).
        then((responseJson) => {
          console.log("image response = ", responseJson)
        }).
        catch((error) => {
          console.log("Error = ", error)
        })
    }

    
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>

          <View style={styles.header}>

            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{alignSelf: 'center', height: 40, width: 40, justifyContent: 'center'}}>
                <Image 
                    source={require('../../../assets/images/backButton.png')}
                    style={{
                        height: 20, 
                        width: 10,
                        alignSelf: 'center',
                    }}
                />
            </TouchableOpacity>
              

            <Text style={styles.addPetText}>
                Add a pet
            </Text>

          </View>

          <Text style={styles.informationText}>
            Step 1/2: general information
          </Text>
          
          <View style={styles.inputContainer}>

            <TouchableOpacity style={{alignSelf: 'center'}} onPress={() => this.showAlert()}>
              <Image 
              source={this.renderSourceImage()}
              style={{
                  width: 60, 
                  height: 65,
                  alignSelf: 'center'
              }}
              />
            </TouchableOpacity>

            <View style={styles.inputTextContainer}>

                <Text style={{color: '#FAAC75', fontSize: 16, fontFamily: 'Roboto-Medium'}}>
                    Name
                </Text>

                <TextInput 
                  style={styles.textInput}
                  value={this.state.name}
                  onChangeText={(text) => this.setState({name: text})}
                />

            </View>

          </View>

          <Text style={{color: '#A5A0A0', fontSize: 16, fontFamily: 'Roboto-Medium', marginTop: 30,}}>
            Type
          </Text>

          <View style={styles.typeContainer}>

            <TouchableOpacity style={[styles.type, {backgroundColor: this.getBackgroundColor('cat')}]} onPress={() => this.chooseType('cat')}>

                <Image 
                    source={require('../../../assets/images/catIcon.png')}
                    style={{
                        alignSelf: 'center', 
                        tintColor: this.getTintColor('cat')
                    }}
                />
                
            </TouchableOpacity>

            <TouchableOpacity style={[styles.type, {backgroundColor: this.getBackgroundColor('dog')}]} onPress={() => this.chooseType('dog')}>

                <Image 
                    source={require('../../../assets/images/dogIcon.png')}
                    style={{
                        alignSelf: 'center', 
                        tintColor: this.getTintColor('dog')
                    }}
                />

            </TouchableOpacity>

          </View>

        </View>


        <View style={styles.bottomTab}>

          {this.renderKeyboardButton()}

          <TouchableOpacity style={[styles.nextButton, {backgroundColor: this.getNextBackground()}]} onPress={() => this.handleNext()}>

            <Text style={{
              color: 'white',
              alignSelf: 'center'
            }}>
              Update
            </Text>

          </TouchableOpacity>

        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#F5F5F9', 
    justifyContent: 'flex-start'
  }, 
  body: {
    ...ifIphoneX({
        marginTop: 50,
    }),
    width: width * 0.9, 
    alignSelf: 'center'
  }, 
  header: {
    flexDirection: 'row',
    textAlign: 'center',
    marginTop: 30,
    
  }, 
  addPetText: {
    color: '#333333', 
    fontSize: 24,
    fontFamily: 'Roboto-Medium', 
    alignSelf: 'center', 
    marginLeft: 50,
  }, 
  informationText: {
    fontSize: 18,
    color: '#333333', 
    fontFamily: 'Roboto-Medium',
    marginTop: 20,
  }, 
  inputContainer: {
    marginTop: 20,
    flexDirection: 'row', 
  },
  inputTextContainer: {
    marginLeft: 10,
    width: (width * 0.9) - 75, 
    justifyContent: 'space-between'
  }, 
  textInput: {
    borderBottomColor: '#FAAC75', 
    borderBottomWidth: 1,
    width: '100%', 
    color: 'black'
  }, 
  typeContainer: {
    marginTop: 10,
    flexDirection: 'row', 
    justifyContent: 'space-around'
  }, 
  type: {
    width: 135, 
    height: 65,
    borderRadius: 10, 
    justifyContent: 'center'
  }, 
  bottomTab: {
    // ...ifIphoneX({
    //   height: 100,
    // }, {
    //   height: 75,
    // }), 
    height: 75,
    width: width,
    backgroundColor: 'white', 
    position: 'absolute', 
    bottom: 0,
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center', 
  }, 
  nextButton: {
    width: 70, 
    height: 40, 
    borderRadius: 6, 
    ...ifIphoneX({
      marginRight: 40,
    }, {
      marginRight: 25
    }),
    justifyContent: 'center'
  }
})

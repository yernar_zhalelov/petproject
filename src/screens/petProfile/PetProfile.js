import React from 'react'
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  TouchableOpacity, 
  TextInput, 
  Alert, 
  Platform, 
  ToastAndroid,
  Keyboard, 
  FlatList, 
  Animated
} from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class PetProfileScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      pet: props.route.params.pet, 
      freq_type: {}, 
      procedure_type: {}, 
      freq_number: {}, 
    }
  }

  takeFreqType = (type) => {
    switch(type) {
      case 1: 
        return 'a day'
      case 2: 
        return 'a week'
      case 3: 
        return 'a month'
      case 4: 
        return 'a quarter'
      case 5: 
        return 'a year'
    }
  }

  takeProcedureType = (type) => {
    switch(type) {
      case 1: 
        return 'Vaccination'
      case 2: 
        return 'Deworming'
      case 3: 
        return 'Medication'
      case 4: 
        return 'Grooming'
    }
  }

  renderType = (type) => {
    if(type == 1) {
      return 'Cat'
    } else {
      return 'Dog'
    }
  }

  renderImage = (image) => {
    if(image == null) {
      return require('../../../assets/images/inputPhoto.png')
    } else {
      return {uri: image.url}
    }
  }

  listNearestEvents = () => {
    let events = this.state.pet.procedures

    if(events == []) {
        return null
    } else {
        return (
            <View style={styles.nearestEventContainer}> 

                <Image 
                    source={require('../../../assets/images/greenCircle.png')}
                    style={{
                        margin: 5
                    }}
                />

                <View style={{
                    marginLeft: 10
                }}>

                    <Text style={{
                        fontSize: 16, 
                        fontFamily: 'Roboto-Medium', 
                        marginTop: 5, 
                        color: '#333333'
                    }}>
                        Medication
                    </Text>

                    <Text style={{
                        fontSize: 14, 
                        fontFamily: 'Roboto-Regular', 
                        marginTop: 5, 
                        color: '#333333'
                    }}>
                        Tomorrow, Avamir, 200 grams
                    </Text>

                </View>

            </View>
        )
    }
  }

  listProcedures = () => {
      if(this.state.pet.procedures == []) {
        return null
      } else {

        let result = this.state.pet.procedures

        return (
            <FlatList 
                data={result}
                renderItem={({item}) => this.renderProcedure(item)}
            />
        )

      }
  }

  renderProcedure = (item) => {
    return (
        <View style={styles.procedureContainer}>

            <View style={{
                margin: 10
            }}>

                <Text style={{
                    fontSize: 16, 
                    fontFamily: 'Roboto-Medium', 
                    color: '#333333'
                }}>
                    {this.takeProcedureType(item.procedure_type)}
                </Text>

                <Text style={{
                    fontSize: 14, 
                    fontFamily: 'Roboto-Regular', 
                    color: '#333333', 
                    marginTop: 5
                }}>
                    {item.frequency_number} time(s) {this.takeFreqType(item.frequency_type)}, {item.name}, {item.dose}. Last event - {item.last_procedure.substring(0, 10)}
                </Text>

            </View>


        </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>

          <View style={styles.header}>

            <View style={{flexDirection: 'row'}}>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{alignSelf: 'center', height: 40, width: 40, justifyContent: 'center'}}>
                    <Image 
                        source={require('../../../assets/images/backButton.png')}
                        style={{
                            height: 20, 
                            width: 10,
                            alignSelf: 'center',
                        }}
                    />
                </TouchableOpacity>
                
                <Text style={styles.addPetText}>
                    Pet's profile
                </Text>
            </View>
            
          </View>

          <View style={styles.profileView}>

            <View style={{flexDirection: 'row', width: '90%'}}>

                <Image 
                    source={this.renderImage(this.state.pet.image)}
                    style={styles.petImage}
                />

                <View style={{marginLeft: 20}}>

                    <Text style={{
                        color: '#333333', 
                        fontSize: 18, 
                        fontFamily: 'Roboto-Medium',
                    }}
                    >
                        {this.state.pet.name}
                    </Text>

                    <Text style={{
                        color: '#333333', 
                        fontSize: 12, 
                        fontFamily: 'Roboto-Regular'
                    }}>
                        {this.renderType(this.state.pet.type)}
                    </Text>

                </View>

            </View>

            <TouchableOpacity style={styles.updateButton} onPress={() => this.props.navigation.navigate('UpdatePet', {pet: this.state.pet})}>
                <Image 
                    source={require('../../../assets/images/updateButton.png')}
                    style={{
                      
                    }}
                />
            </TouchableOpacity>
            
            
          </View>

          <View style={styles.nearestEventsView}>

            <Text style={styles.viewText}>

                Nearest events

            </Text>

            {this.listNearestEvents()}

          </View>

          <View style={styles.proceduresView}> 

            <View style={{
                flexDirection: 'row', 
                justifyContent: 'space-between'
            }}>

                <Text style={styles.viewText}>
                    Procedures
                </Text>

                <TouchableOpacity>

                    <Image 
                        source={require('../../../assets/images/updateButton.png')}
                    />

                </TouchableOpacity>


            </View>

            {this.listProcedures()}

          </View>


        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#F5F5F9', 
    justifyContent: 'flex-start'
  }, 
  body: {
    ...ifIphoneX({
        marginTop: 50,
    }),
    width: width * 0.9, 
    alignSelf: 'center'
  }, 
  header: {
    flexDirection: 'row',
    textAlign: 'center',
    marginTop: 30,
    justifyContent: 'space-between'
  }, 
  addPetText: {
    color: '#333333', 
    fontSize: 24,
    fontFamily: 'Roboto-Medium', 
    alignSelf: 'center', 
    marginLeft: 50,
  }, 
  profileView: {
    flexDirection: 'row',
    justifyContent: 'space-between', 
    marginTop: 35, 
  }, 
  petImage: {
    width: 75, 
    height: 85,
  },
  nearestEventsView: {
    marginTop: 30
  }, 
  proceduresView: {
    marginTop: 20,
  }, 
  updateButton: {
    
  }, 
  viewText: {
    fontSize: 18, 
    fontFamily: 'Roboto-Medium', 
    color: '#333333'
  }, 
  nearestEventContainer: { 
    flexDirection: 'row', 
    marginTop: 20, 
    backgroundColor: '#FFFFFF', 
    borderRadius: 10,
    height: 70,
  }, 
  procedureContainer: { 
    marginTop: 10, 
    backgroundColor: '#FFFFFF', 
    borderRadius: 10,
    height: 85,
  }
})

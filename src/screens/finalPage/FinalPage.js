import React from 'react'
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  TouchableOpacity, 
  TextInput, 
  Alert, 
  Platform, 
  ToastAndroid,
  Keyboard
} from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height


export default class FinalPageScreen extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);

  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>

         
        <Text style={styles.text}>
        Great!
        Your pet has been added. Now you won't miss any procedures 🤗
        </Text>
             
        </View>

        <TouchableOpacity style={styles.saveButton} onPress={() => this.props.navigation.reset({
            index: 0,
            routes: [{name: 'Tab'}],
        })}>
            <Text style={styles.saveText}>
                Got it
            </Text>
        </TouchableOpacity>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#F5F5F9', 
    justifyContent: 'flex-start'
  }, 
  body: {
    ...ifIphoneX({
        marginTop: 50,
    }),
    width: width * 0.9, 
    alignSelf: 'center'
  }, 
  text: {
    width: width * 0.85, 
    fontSize: 24, 
    fontFamily: 'Roboto-Regular',
    color: '#333333',
    alignSelf: 'center', 
    marginTop: 160
  },
  saveButton: {
    borderRadius: 10,
    height: 54, 
    backgroundColor: '#FAAC75', 
    shadowOffset: {
      width: 0,
      height: 4,
    }, 
    shadowColor: '#000000', 
    shadowOpacity: 0.5,
    shadowRadius: 5,
    justifyContent: 'center', 
    elevation: 10,
    position: 'absolute', 
    bottom: 35,
    width: '90%', 
    alignSelf: 'center'
  }, 
  saveText: {
    fontFamily: 'Roboto-Medium', 
    color: '#333333', 
    fontSize: 18,
    alignSelf: 'center'
  },
})

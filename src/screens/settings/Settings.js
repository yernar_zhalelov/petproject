import AsyncStorage from '@react-native-async-storage/async-storage';
import React from 'react'
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Image
} from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { Switch } from 'react-native-paper';

import { url } from '../../AppConfig';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class SettingsScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      switchValue: false
    }

    AsyncStorage.getItem('notifs').then((notifs) => {
      if(notifs == 'true') {
        this.setState({switchValue: true})
      } else {
        this.setState({switchValue: false})
      }
    })
  }
  
  toggleSwitch = (token) => {

    if(this.state.switchValue == true) {

      fetch(url + `/api/delete-token?token=${token}`, {
        headers: {
          'Content-Type':'text/html'
        },
        method: 'GET', 
      }). 
      then((response) => response.text()).
      then(responseJson => {
        console.log("response on off = ", responseJson)
        
        AsyncStorage.setItem('notifs', 'false')
        this.setState({switchValue: false})
  
      }).
      catch((error) => {
        console.log("Error storing = ", error)
      })
        
    } else {

      AsyncStorage.getItem('fbToken').then((key) => {
        fetch(url + `/api/store-token?token=${token}&device_key=${key}`, {
          headers: {
            'Content-Type':'text/html'
          },
          method: 'GET', 
        }). 
        then((response) => response.text()).
        then(responseJson => {
          console.log('response on on = ', responseJson)
          
          AsyncStorage.setItem('notifs', 'true')
          this.setState({switchValue: true})
    
        }).
        catch((error) => {
          console.log("Error storing = ", error)
        })
      })
      
    }

  }

  render() {
    return (
      <View style={styles.container}>
         <View style={styles.body}>

          <Text style={styles.settingsText}>
            Settings
          </Text>

          <View style={styles.notificationContainer}>

            <View style={{flexDirection: 'row', }}>
              <Image 
                source={require('../../../assets/images/notificationIcon.png')}
                style={{
                  width: 17,
                  height: 18
                }}
              />

              <Text style={styles.notificationText}>
                Push-notification
              </Text>
            </View>

            

            <Switch 
              value={this.state.switchValue} 
              onValueChange={() => {
                AsyncStorage.getItem('token').then((token) => {
                  this.toggleSwitch(token)
                })
              }} 
              color='#FAAC75'
              style={{ transform: [{ scaleX: 1.3 }, { scaleY: 1.3 }] }}
            />
          </View>

          <View style={styles.contactContainer}>

            <Image 
              source={require('../../../assets/images/contactIcon.png')}
              style={{
                width: 20,
                height: 20,
              }}
            />

            <Text style={styles.notificationText}>
              Contact us
            </Text>

          </View>

         </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#F5F5F9', 
    justifyContent: 'flex-start'
  }, 
  body: {
    ...ifIphoneX({
        marginTop: 50,
    }),
    width: width * 0.8,
    alignSelf: 'center',
  }, 
  settingsText: {
    color: '#333333', 
    fontSize: 24, 
    fontFamily: 'Roboto-Medium', 
    marginTop: 30,
  }, 
  notificationContainer: {
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center', 
    marginTop: 30,
  }, 
  notificationText: {
    fontSize: 16, 
    color: '#333333', 
    fontFamily: 'Roboto-Regular',
    marginLeft: 15,
  }, 
  contactContainer: {
    flexDirection: 'row', 
    marginTop: 30,
    alignItems: 'center',
  }
})

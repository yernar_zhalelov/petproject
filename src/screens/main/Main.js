import React from 'react'
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Image, 
  TouchableOpacity, 
  FlatList, 
  ScrollView, 
  ToastAndroid,
  BackHandler,
  SafeAreaView, 
  LogBox
} from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper';
import {url} from '../../AppConfig'
import { firebase } from '@react-native-firebase/analytics';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TestIds, BannerAd, BannerAdSize} from '@react-native-firebase/admob';
import { Path } from 'react-native-svg';

import PetCard from '../../components/PetCard';
import EventCard from '../../components/EventCard';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class MainScreen extends React.Component {
  constructor(props) {
    super(props)

    this.state={
      pets: null,
      procedures: null,
      events: null,
      ad: false,
    }

  }

  handleAllPets = () => {
    if(this.state.pets != null && this.state.pets.toString().trim() != []) {
      this.props.navigation.navigate('AllPets', {pets: this.state.pets})
    }
  }

  getProcedures = (token) => {
    console.log(token)
    fetch(url + `/api/procedures?token=${token}`, {
      method: 'GET', 
      headers: {}, 
    }).then((response) => response.json()).
    then((responseJson) => {
      this.setState({procedures: responseJson.data})
    }).
    catch((error) => {
      console.log('error procedure = ', error)
    })
  }

  getPets = (token) => {

    fetch(url + `/api/pets?token=${token}`, {
      method: 'GET', 
      headers: {}, 
    }).then((response) => response.json()).
    then((responseJson) => {

      this.setState({pets: responseJson.data})
    }).
    catch((error) => {
      console.log('error ha = ', error)
    })
  }

  getEvents = (token) => {
    fetch(url + `/api/notifications?token=${token}`, {
      method: 'GET', 
      headers: {}, 
    }).then((response) => response.json()).
    then((responseJson) => {

      this.setState({events: responseJson.data})
    }).
    catch((error) => {
      console.log('error ha = ', error)
    })
  }

  componentDidMount() {
    AsyncStorage.getItem('token').then((token) => {
      this.getPets(token)
      this.getProcedures(token)
      this.getEvents(token)
    })

    this.props.navigation.addListener('focus', () => {
      AsyncStorage.getItem('token').then((token) => {
        this.getPets(token)
        this.getProcedures(token)
        this.getEvents(token)
      })
    })

    console.log('id - ', TestIds.BANNER)
  }


  renderPetsContainer = () => {
    if(this.state.pets == null || this.state.pets.toString().trim() == []) {
      return (
        <Text style={styles.noPetsText}>
          You haven't added any pets yet 😔
        </Text> 
      )
    } else {
        return (
          <SafeAreaView style={{flex: 1}}>
            <FlatList 
              data={this.state.pets.slice(0, 2)}
              renderItem={({item, index}) => <PetCard item={item} navigation={this.props.navigation}/>}
            />
          </SafeAreaView>
        )
    }
  }

  renderEventsContainer = () => {

    if(this.state.events == null || this.state.events.toString().trim() == []) {
      return (
        <View style={styles.eventsContainer}>

          <View style={styles.greenCircleContainer}>
            <Image 
                source={require('../../../assets/images/greenCircle.png')}
                style={{
                  margin: 10,
                }}
            />
          </View>
            

          <Text style={styles.eventsText}>
            Information about upcoming procedures will appear here
          </Text>

        </View>
      )
    } else {
      return (
        <FlatList 
          data={this.state.events}
          renderItem={({item, index}) => <EventCard item={item}/>}
        />
      )
    }
  }

  renderColorAll = () => {
    if(this.state.pets == null || this.state.pets.toString().trim() == []) {
      return {color: '#979797'}
    } else {
      return {color: '#FAAC75'}
    }
  }

  renderAd = () => {
    if(this.state.ad == false) {
      return (
        <BannerAd
          unitId={'ca-app-pub-3940256099942544/6300978111'}
          size={BannerAdSize.ADAPTIVE_BANNER}
        />
      ) 
    } else {
      return (
        <BannerAd
          unitId={'ca-app-pub-3940256099942544/6300978111'}
          size={BannerAdSize.ADAPTIVE_BANNER}
        />
      ) 
    }
  }

  render() {
    return (
      <View style={styles.container}>
         <ScrollView style={styles.body} showsVerticalScrollIndicator={false}>

           <Text style={styles.hiText}>
             Hi!
           </Text>

           <Text style={styles.importantEventsText}>
             Important events
           </Text>


           {this.renderEventsContainer()}

           <View style={{flexDirection: 'row', justifyContent: 'space-between',}}>
              <Text style={styles.myPetsText}>
                My pets
              </Text>

              <TouchableOpacity style={{alignSelf: 'flex-end', height: 30, width: 30, alignItems: 'flex-start'}} onPress={() => this.handleAllPets()}>
                <Text style={[styles.allText, this.renderColorAll()]}>
                  All
                </Text>
              </TouchableOpacity>
              
           </View>

            {this.renderPetsContainer()}
            <TouchableOpacity
              onPress={async () => {
                this.props.navigation.navigate('AddPet')
                await firebase.analytics().logEvent('app_a_pet', {
                  id: 1
                })
              }}
              style={styles.addButton}
            >
              <Text style={styles.addText}>
                Add
              </Text>
            </TouchableOpacity>

         </ScrollView>

         <View style={styles.adView}>
          {this.renderAd()}
         </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#F5F5F9', 
    justifyContent: 'flex-start'
  }, 
  body: {
    ...ifIphoneX({
        marginTop: 50,
    }),
    width: width * 0.9, 
    alignSelf: 'center',
  }, 
  hiText: {
    fontSize: 20, 
    fontFamily: 'Roboto-Regular',
    color: '#333333', 
    marginTop: 20,
  }, 
  importantEventsText: {
    fontSize: 24, 
    fontFamily: 'Roboto-Medium', 
    color: '#333333', 
    marginTop: 30,
  },  
  myPetsText: {
    fontSize: 24, 
    fontFamily: 'Roboto-Medium', 
    color: '#333333', 
    marginTop: 30,
  }, 
  allText: { 
    fontSize: 16, 
    fontFamily: 'Roboto-Regular',
    alignSelf: 'flex-end'
  },
  noPetsText: {
    fontSize: 16, 
    fontFamily: 'Roboto-Regular', 
    color: '#727070', 
    marginTop: 15,
  }, 
  addButton: {
    borderRadius: 10,
    height: 54, 
    backgroundColor: '#FAAC75', 
    marginTop: 20,
    shadowOffset: {
      width: 0,
      height: 4,
    }, 
    shadowColor: '#000000', 
    shadowOpacity: 0.5,
    shadowRadius: 5,
    justifyContent: 'center', 
    elevation: 10,
    marginBottom: 70,
  }, 
  addText: {
    fontFamily: 'Roboto-Medium', 
    color: '#333333', 
    fontSize: 18,
    alignSelf: 'center'
  }, 
  adView: {
    position: 'absolute', 
    bottom: 0,
    flex: 1,
  },
  eventsText: {
    fontSize: 16, 
    fontFamily: 'Roboto-Regular', 
    color: '#333333',  
    flex: 7,
    marginTop: 10,
  },
  eventsContainer: {
    backgroundColor: 'white', 
    borderRadius: 15,
    height: 75,
    flexDirection: 'row',
    justifyContent: 'center', 
    marginTop: 20,
  }, 
  greenCircleContainer: {
    flex: 1, 
  },
})

import React from 'react'
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  TouchableOpacity, 
  TextInput, 
  Alert, 
  Platform, 
  ToastAndroid,
  Keyboard, 
  FlatList, 
  Animated
} from 'react-native'
import { ifIphoneX } from 'react-native-iphone-x-helper';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import {url} from '../../AppConfig'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { firebase } from '@react-native-firebase/analytics';

import PetCard from '../../components/PetCard';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class AllPetsScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state={
      pets: props.route.params.pets,
    }
  }

  renderImage = (image) => {
    if(image == null) {
      return require('../../../assets/images/inputPhoto.png')
    } else {
      return {uri: image.url}
    }
  }

  renderType = (type) => {
    if(type == 1) {
      return 'Cat'
    } else {
      return 'Dog'
    }
  }

  renderRightActions = (item) => {

    return (
      <TouchableOpacity
      style={{
        backgroundColor: '#FAAC75',
        justifyContent: 'center',
        alignItems: 'flex-end',
        alignSelf: 'flex-end',
        height: 75,
        borderTopRightRadius: 10, 
        borderBottomRightRadius: 10
      }}
      onPress={() => this.deletePet(item)}
    >
      <Text
        style={{
          color: '#FFFFFF',
          paddingHorizontal: 10,
          fontWeight: '600',
          paddingHorizontal: 30,
          paddingVertical: 20,
        }}
      >
        Delete
      </Text>
    </TouchableOpacity>
    );
  };

  renderPet = (item) => {
    return (
    <TouchableOpacity onPress={() => this.props.navigation.navigate('PetProfile', {pet: item})}>
      <Swipeable 
        renderRightActions={() => this.renderRightActions(item)}
        rightThreshold={10}
        overshootRight={false}
      >
        <View style={styles.petContainer}>
          <Image 
            source={this.renderImage(item.image)}
            style={{
              width: 60, 
              height: 60,
              marginHorizontal: 15,
            }}
          />

          <View>

            <Text style={{
              color: '#333333', 
              fontSize: 14,
              fontFamily: 'Roboto-Medium'
            }}>
              {item.name}
            </Text>

            <Text style={{
              color: '#333333', 
              fontFamily: 'Roboto-Regular', 
              fontSize: 12
            }}>
              {this.renderType(item.type)}
            </Text>

          </View>
        </View>
        </Swipeable>
      </TouchableOpacity>
    )
  }

  deletePet = (item) => {

    AsyncStorage.getItem('token').then((token) => {
      
      fetch(url + '/api/pets/' + item.id + '?token=' + token, {
        method: 'DELETE'
      }).
      then((response) => response.json()).
      then((responseJson) => {
        console.log(responseJson)
  
        let index = this.state.pets.indexOf(item)
        this.state.pets.splice(index, 1)
        
        this.setState({pets: this.state.pets})
  
      }).
      catch((error) => {
        console.error("Error in deleting = ", error)
      })

    })

  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>

          <View style={styles.header}>

            <View style={{flexDirection: 'row'}}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{alignSelf: 'center', height: 40, width: 40, justifyContent: 'center'}}>
                    <Image 
                        source={require('../../../assets/images/backButton.png')}
                        style={{
                            height: 20, 
                            width: 10,
                            alignSelf: 'center',
                        }}
                    />
                </TouchableOpacity>
                
                <Text style={styles.addPetText}>
                    My pets
                </Text>
            </View>


            <TouchableOpacity 
              onPress={async () => {
                this.props.navigation.navigate('AddPet')
                await firebase.analytics().logEvent('app_a_pet', {
                  id: 1
                })
              }}
            >
                <Image 
                    source={require('../../../assets/images/addButton.png')}
                    style={{
                        width: 48,
                        height: 48,
                        
                    }}
                />
            </TouchableOpacity>
            

          </View>

          <FlatList 
            data={this.state.pets}
            renderItem={(item) => this.renderPet(item.item)}
          />


        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#F5F5F9', 
    justifyContent: 'flex-start'
  }, 
  body: {
    ...ifIphoneX({
        marginTop: 50,
    }),
    width: width * 0.9, 
    alignSelf: 'center'
  }, 
  header: {
    flexDirection: 'row',
    textAlign: 'center',
    marginTop: 30,
    justifyContent: 'space-between'
  }, 
  addPetText: {
    color: '#333333', 
    fontSize: 24,
    fontFamily: 'Roboto-Medium', 
    alignSelf: 'center', 
    marginLeft: 50,
  }, 
  petContainer: {
    borderRadius: 10,
    backgroundColor: 'white', 
    height: 75, 
    width: '100%', 
    flexDirection: 'row', 
    alignItems: 'center', 
    marginTop: 15,
    shadowOffset: {
      width: 0,
      height: 2,
    }, 
    shadowColor: '#000000', 
    shadowOpacity: 0.5,
    shadowRadius: 15,
    elevation: 1,
  }, 
  actionText: {
    color: 'black',  
    alignSelf: 'flex-end'
  }, 
  leftAction: {
    backgroundColor: '#FAAC75', 
    height: 75,
    alignSelf: 'flex-end',
    width: 100,
    justifyContent: 'center', 
    textAlign: 'right'
  }
})
